import os

import pandas as pd
import numpy as np
from functions_dbPreparation import getLabelFromLabelAbbrevation_EmoDB, getOneHotEncodedDataFrame
from functions_preprocessing import getMFCCs, getMeanMFCCs, getMeanMelSpectrogramValues, getMelSpectrogramValues

EmoDBpath = "./databases/Emo-DB/"
EmoDBfilePaths = []
EmoDBlabels = []
EmoDBtable = pd.DataFrame()
features = pd.DataFrame(columns=["features"])

for file in os.listdir(EmoDBpath):
    labelAbbrevation = file[5]
    label = getLabelFromLabelAbbrevation_EmoDB(labelAbbrevation)
    if label != "unknown":
        EmoDBfilePaths.append(EmoDBpath + file)
        EmoDBlabels.append(label)

EmoDBtable["filepath"] = EmoDBfilePaths
EmoDBtable["label"] = EmoDBlabels
EmoDBtable = getOneHotEncodedDataFrame(EmoDBtable)

for index, filepath in enumerate(EmoDBtable.filepath):
    # Deep learning preparation
    # mfcc = getMFCCs(filepath)
    melFeatures = getMelSpectrogramValues(filepath)
    features.loc[index] = [melFeatures.flatten()]

    # Classic machine learning preparation
    # meanMFCCs = getMeanMFCCs(filepath)
    # meanMelFeatures = getMeanMelSpectrogramValues(filepath)
    # features.loc[index] = [meanMelFeatures]
    print(index)

EmoDBtable = pd.concat([EmoDBtable, pd.DataFrame(features["features"].values.tolist())], axis=1)
# Saving data for deep learning
EmoDBtable.to_csv('./databases/EmoDBTable_MEL_DL.csv', index=False)
# Saving data for classic machine learning
# EmoDBtable.to_csv('./databases/EmoDBTable_MFCC_ML.csv', index=False)
