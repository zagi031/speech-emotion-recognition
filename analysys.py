import json
import numpy as np
from functionsPerformanceAnalysys import plotWhiskerPlot


def avg(obj):
    return sum(obj) / len(obj)


def getAccuracies(reports):
    accuracies = []
    for report in reports:
        accuracies.append(report["accuracy"])
    return accuracies


def getMacroMeasures(reports, measureKey):
    macroMeasures = []
    for report in reports:
        macroMeasures.append(report["macro avg"][measureKey])
    return macroMeasures


def getMeanEmotionMeasure(reports, emotionKey, measureKey):
    measures = []
    for report in reports:
        measures.append(report[emotionKey][measureKey])
    return np.mean(measures)


filePath1 = "./results/rf_rav.json"
file1 = open(filePath1)
reports1 = json.load(file1)
#
f1Scores1 = getMacroMeasures(reports1, "f1-score")

filePath2 = "./results/rf_emo.json"
file2 = open(filePath2)
reports2 = json.load(file2)
#
f1Scores2 = getMacroMeasures(reports2, "f1-score")

filePath3 = "./results/svm_rav.json"
file3 = open(filePath3)
reports3 = json.load(file3)
#
f1Scores3 = getMacroMeasures(reports3, "f1-score")

filePath4 = "./results/svm_emo.json"
file4 = open(filePath4)
reports4 = json.load(file4)
#
f1Scores4 = getMacroMeasures(reports4, "f1-score")

filePath5 = "./results/cnn_rav.json"
file5 = open(filePath5)
reports5 = json.load(file5)
#
f1Scores5 = getMacroMeasures(reports5, "f1-score")

filePath6 = "./results/cnn_emo.json"
file6 = open(filePath6)
reports6 = json.load(file6)
#
f1Scores6 = getMacroMeasures(reports6, "f1-score")

plotWhiskerPlot((f1Scores1, f1Scores2, f1Scores3, f1Scores4, f1Scores5, f1Scores6),
                ["RF-RAVDESS", "RF-EmoDB", "SVM-RAVDESS", "SVM-EmoDB", "CNN-RAVDESS", "CNN-EmoDB"], "F1")

# f1scores = getMacroMeasures(reports, "f1-score")
# recalls = getMacroMeasures(reports, "recall")
# precisions = getMacroMeasures(reports, "precision")
# print("acc:\t", np.mean(accuracies), "+-", np.std(accuracies))
# print("f1:\t", np.mean(f1scores), "+-", np.std(f1scores))
# print("recalls:\t", np.mean(recalls), "+-", np.std(recalls))
# print("precs:\t", np.mean(precisions), "+-", np.std(precisions))
# #
# print("\n")
# angerMeanF1 = getMeanEmotionMeasure(reports, "0", "f1-score")
# disgustMeanF1 = getMeanEmotionMeasure(reports, "1", "f1-score")
# fearMeanF1 = getMeanEmotionMeasure(reports, "2", "f1-score")
# happyMeanF1 = getMeanEmotionMeasure(reports, "3", "f1-score")
# neutralMeanF1 = getMeanEmotionMeasure(reports, "4", "f1-score")
# sadMeanF1 = getMeanEmotionMeasure(reports, "5", "f1-score")
# print("angerMeanF1:\t", angerMeanF1)
# print("disgustMeanF1:\t", disgustMeanF1)
# print("fearMeanF1:\t", fearMeanF1)
# print("happyMeanF1:\t", happyMeanF1)
# print("neutralMeanF1:\t", neutralMeanF1)
# print("sadMeanF1:\t", sadMeanF1)

xx = 5
