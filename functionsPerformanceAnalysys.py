import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from sklearn.model_selection import RandomizedSearchCV
from tabulate import tabulate


def plotCFMatrix(cf_matrix, classes):
    ax = sns.heatmap(cf_matrix, annot=True, cmap='Blues')
    ax.set_title('Emotion Recognition Confusion Matrix with labels\n\n')
    ax.set_xlabel('\nPredicted Emotion Class')
    ax.set_ylabel('Actual Emotion Class ')
    ax.xaxis.set_ticklabels(classes)
    ax.yaxis.set_ticklabels(classes)
    plt.show()


def printBestParamsFromHyperparameterTuning(grid, classifier, X_train, y_train, n_iter, cv=10):
    cf_random = RandomizedSearchCV(estimator=classifier, param_distributions=grid, n_iter=n_iter, n_jobs=-1, cv=cv,
                                   scoring='neg_log_loss', verbose=2)
    cf_random.fit(X_train, y_train)
    print(cf_random.best_params_)


def printMeanAndStdDevResults(train_accuracy_scores, validation_accuracy_scores, train_log_loss_scores,
                              validation_log_loss_scores):
    data = [["Mean accuracy", np.mean(train_accuracy_scores), np.mean(validation_accuracy_scores)],
            ["Standard deviation of accuracy", np.std(train_accuracy_scores), np.std(validation_accuracy_scores)],
            ["Mean log loss", np.mean(train_log_loss_scores), np.mean(validation_log_loss_scores)],
            ["Standard deviation of log loss", np.std(train_log_loss_scores), np.std(validation_log_loss_scores)]]
    headers = ["", "Training", "Validation"]
    print(tabulate(data, headers), "\n")


def plotCrossValidationAnalysys(training_accuracy_scores, test_accuracy_scores, training_loss_scores, test_loss_scores,
                                k_folds):
    plt.subplot(1, 2, 1)
    plt.plot(range(1, k_folds + 1), training_accuracy_scores, 'o-', label="training")
    plt.plot(range(1, k_folds + 1), test_accuracy_scores, 'o-', label="validation")
    plt.legend(loc="lower right")
    plt.xlabel('indeks preklopa')
    plt.ylabel('točnost')
    plt.ylim(0, 1)
    plt.title('Točnost po preklopu')
    plt.tight_layout()
    plt.subplot(1, 2, 2)
    plt.plot(range(1, k_folds + 1), training_loss_scores, 'o-', label="training")
    plt.plot(range(1, k_folds + 1), test_loss_scores, 'o-', label="validation")
    plt.legend(loc="lower right")
    plt.xlabel('indeks preklopa')
    plt.ylabel('logaritamski gubitak')
    plt.ylim(0, 1.5)
    plt.title('Logaritamski gubitak unakrsne validacije po preklopu')
    plt.tight_layout()
    plt.show()


def plotWhiskerPlot(data, labels, yLabel):
    fig = plt.figure(figsize=(10, 7))
    plt.boxplot(data)
    plt.xticks([x for x in range(1, len(labels)+1)], labels)
    plt.ylabel(yLabel)
    plt.show()
