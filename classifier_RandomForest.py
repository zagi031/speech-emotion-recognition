import json
import joblib
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, log_loss, precision_score, \
    recall_score, f1_score
from sklearn.model_selection import KFold, train_test_split
from functionsPerformanceAnalysys import plotCFMatrix, printBestParamsFromHyperparameterTuning, \
    plotCrossValidationAnalysys, printMeanAndStdDevResults

data = pd.read_csv("./databases/EmoDBTable_MFCC_ML.csv")
target = data["label"]
data = data.drop(["label", "filepath"], axis=1)
# X_data, X_test, y_data, y_test = train_test_split(data, target, test_size=0.2,
#                                                   random_state=0, shuffle=True, stratify=target)

reports = []
for i in range(0, 20):
    X_data, X_test, y_data, y_test = train_test_split(data, target, test_size=0.2, shuffle=True, stratify=target)

    # training_accuracy_scores = []
    # training_log_loss_scores = []
    # validation_accuracy_scores = []
    # validation_log_loss_scores = []
    #
    # param_grid = {'n_estimators': [100, 150, 200],
    #               'max_features': ['auto', 'sqrt'],
    #               'max_depth': [50, 60, 70, 80, 90, 100, 110, 120],
    #               'min_samples_split': [2, 4, 6, 8, 10],
    #               'min_samples_leaf': [1, 2, 3, 4]}
    #
    # # hyperparameter tuning
    # # printBestParamsFromHyperparameterTuning(param_grid, RandomForestClassifier(), X_data, y_data, 200)
    # # MFCC
    # # Emo-DB {'n_estimators': 150, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'sqrt', 'max_depth': 100}
    # # RAVDESS {'n_estimators': 100, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'auto', 'max_depth': 70}
    # # MEL
    # # Emo-DB {'n_estimators': 150, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'sqrt', 'max_depth': 50}
    # # RAVDESS {'n_estimators': 200, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'sqrt', 'max_depth': 60}
    #
    # # training + validation
    # kf = KFold(n_splits=5, shuffle=True, random_state=None)
    # for train_index, validation_index in kf.split(X_data):
    #     X_train, y_train = data.iloc[train_index], target.iloc[train_index]
    #     X_validation, y_validation = data.iloc[validation_index], target.iloc[validation_index]
    #
    #     classifier = RandomForestClassifier(n_estimators=150,
    #                                         min_samples_split=2, min_samples_leaf=1, max_features='sqrt', max_depth=100)
    #     classifier.fit(X_train, y_train)
    #
    #     predictions = classifier.predict(X_validation)
    #     accuracy = accuracy_score(y_validation, predictions)
    #     print(accuracy)
    #     validation_accuracy_scores.append(accuracy)
    #     training_accuracy_scores.append(accuracy_score(y_train, classifier.predict(X_train)))
    #     training_log_loss_scores.append(log_loss(y_train, classifier.predict_proba(X_train)))
    #     validation_log_loss_scores.append(log_loss(y_validation, classifier.predict_proba(X_validation)))
    #
    # plotCrossValidationAnalysys(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
    #                             validation_log_loss_scores, 5)
    # printMeanAndStdDevResults(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
    #                           validation_log_loss_scores)

    # testing
    # Emo-DB {'n_estimators': 150, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'sqrt', 'max_depth': 100}
    # RAVDESS {'n_estimators': 100, 'min_samples_split': 2, 'min_samples_leaf': 1, 'max_features': 'auto', 'max_depth': 70}
    classifier = RandomForestClassifier(n_estimators=150,
                                        min_samples_split=2, min_samples_leaf=1, max_features='sqrt', max_depth=100)
    classifier.fit(X_data, y_data)
    predictions = classifier.predict(X_test)
    report = classification_report(y_test, predictions, output_dict=True)
    reports.append(report)
    print(i)

outputFile = open("./results/rf_emo.json", "w")
json.dump(reports, outputFile, indent=4)
outputFile.close()
# joblib.dump(classifier, "randomForestClassifier.pkl")
# classifier = joblib.load("randomForestClassifier.pkl")
