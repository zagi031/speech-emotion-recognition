import librosa
import numpy as np
from pydub import AudioSegment

SAMPLE_RATE = 44100  # Hz
SIGNAL_DURATION = 3  # seconds
NUM_OF_SAMPLES = SIGNAL_DURATION * SAMPLE_RATE
NUM_OF_MFCSS = 20


def amplifyVolume(filepath):
    audio = AudioSegment.from_wav(filepath)
    # amplify Volume by 25 dB
    audio = audio + 25
    return audio


def _loadSignal(filepath):
    y, sr = librosa.load(filepath, sr=SAMPLE_RATE)
    # y, _ = librosa.effects.trim(y=y, top_db=20)
    return y


def _modifySignalLength(signal):
    if signal.shape[0] > NUM_OF_SAMPLES:
        signal = signal[0:NUM_OF_SAMPLES]
    elif signal.shape[0] < NUM_OF_SAMPLES:
        num_missingSamples = NUM_OF_SAMPLES - signal.shape[0]
        missingSamples = np.zeros(num_missingSamples)
        signal = np.concatenate((signal, missingSamples))
    return signal


def getMFCCs(filepath):
    y = _loadSignal(filepath)
    y = _modifySignalLength(y)
    mfcc = librosa.feature.mfcc(y=y, sr=SAMPLE_RATE, n_mfcc=NUM_OF_MFCSS)
    return mfcc


def getMFCCsFromData(data):
    y = _modifySignalLength(data)
    mfcc = librosa.feature.mfcc(y=y, sr=SAMPLE_RATE, n_mfcc=NUM_OF_MFCSS)
    return mfcc


def getMelSpectrogramValues(filepath):
    y = _loadSignal(filepath)
    y = _modifySignalLength(y)
    mels = librosa.feature.melspectrogram(y=y, sr=SAMPLE_RATE)
    return librosa.power_to_db(mels, ref=np.max)


def getMeanMFCCs(filepath):
    return np.mean(librosa.feature.mfcc(y=_loadSignal(filepath), sr=SAMPLE_RATE, n_mfcc=NUM_OF_MFCSS), axis=1)


def getMeanMelSpectrogramValues(filepath):
    return np.mean(getMelSpectrogramValues(filepath), axis=1)
