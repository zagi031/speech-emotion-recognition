import random
import librosa
import numpy as np
import pandas as pd
import soundfile as sf
from functions_preprocessing import SAMPLE_RATE, _loadSignal, getMFCCsFromData
from functions_dbPreparation import getOneHotEncodedDataFrame


def add_white_noise(filepath, noise_percentage_factor=0.05):
    signal = _loadSignal(filepath)
    noise = np.random.normal(0, signal.std(), signal.size)
    augmented_signal = signal + noise * noise_percentage_factor
    return augmented_signal


def pitch_scale(filepath):
    signal = _loadSignal(filepath)
    bins_per_octave = 12
    pitch_pm = 1.5
    pitch_change = pitch_pm * 2 * (np.random.uniform())
    data = librosa.effects.pitch_shift(signal.astype('float64'),
                                       sr=SAMPLE_RATE, n_steps=pitch_change,
                                       bins_per_octave=bins_per_octave)
    return data


def random_gain(filepath, min_factor=1.5, max_factor=3):
    signal = _loadSignal(filepath)
    gain_rate = random.uniform(min_factor, max_factor)
    augmented_signal = signal * gain_rate
    return augmented_signal


# # signal, sr = librosa.load("databases/RAVDESS/03-01-03-01-02-01-04.wav", sr=SAMPLE_RATE)
# # augmentedSignal = random_gain(signal)
# # sf.write("augmented_audio.wav", augmentedSignal, 44100)
#
# data = pd.read_csv('./databases/EmoDBTable_MFCC_ML.csv')[["filepath", "label"]]
# features = pd.DataFrame(columns=["features"])
# labels = []
# i = 0
# for _, instance in data.iterrows():
#     y, sr = librosa.load(instance["filepath"], sr=SAMPLE_RATE)
#     features.loc[i] = [getMFCCsFromData(add_white_noise(y)).flatten()]
#     i = i + 1
#     features.loc[i + 1] = [getMFCCsFromData(pitch_scale(y)).flatten()]
#     i = i + 1
#     features.loc[i + 2] = [getMFCCsFromData(random_gain(y)).flatten()]
#     i = i + 1
#     labels.append(instance["label"])
#     labels.append(instance["label"])
#     labels.append(instance["label"])
#     print(i / 3)
#
# table = pd.DataFrame()
# table["label"] = labels
# table = getOneHotEncodedDataFrame(table)
# table = pd.concat([table, pd.DataFrame(features["features"].values.tolist())], axis=1)
#
# table.to_csv('./databases/EmoDBTable_MFCC_DL_DataAug.csv', index=False)
# # primjeniti data augmentation samo na train set, ne i na validation i test setove
