import json

import numpy as np
import pandas as pd
from functions_dbPreparation import getOneHotEncodedDataFrame
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report
from sklearn.model_selection import train_test_split
from tensorflow import keras
from functions_audioAugmentation import add_white_noise, pitch_scale, random_gain
from functions_preprocessing import NUM_OF_MFCSS, getMFCCsFromData, getMFCCs


def predict(model, X, y):
    """Predict a single sample using the trained model
    :param model: Trained classifier
    :param X: Input data
    :param y (int): Target
    """

    # add a dimension to input data for sample - model.predict() expects a 4d array in this case
    X = X[np.newaxis, ...]  # array shape (1, 130, 13, 1)

    # perform prediction
    prediction = model.predict(X)

    # get index with max value
    predicted_index = np.argmax(prediction, axis=1)

    print("Target: {}, Predicted label: {}".format(y, predicted_index))


def plot_history(history):
    """Plots accuracy/loss for training/validation set as a function of the epochs
        :param history: Training history of model
        :return:
    """

    fig, axs = plt.subplots(2)

    # create accuracy sublpot
    axs[0].plot(history.history["accuracy"], label="train accuracy")
    axs[0].plot(history.history["val_accuracy"], label="validation accuracy")
    axs[0].set_ylabel("Accuracy")
    axs[0].legend(loc="lower right")
    axs[0].set_title("Accuracy eval")

    # create error sublpot
    axs[1].plot(history.history["loss"], label="train error")
    axs[1].plot(history.history["val_loss"], label="validation error")
    axs[1].set_ylabel("Error")
    axs[1].set_xlabel("Epoch")
    axs[1].legend(loc="upper right")
    axs[1].set_title("Error eval")

    plt.show()


def reshapeSets(X_train, X_test, X_validation, y_train, y_test, y_validation):
    X_train = np.array(X_train).reshape((X_train.shape[0], int(X_train.shape[1] / NUM_OF_MFCSS), NUM_OF_MFCSS))
    X_test = np.array(X_test).reshape((X_test.shape[0], int(X_test.shape[1] / NUM_OF_MFCSS), NUM_OF_MFCSS))
    X_validation = np.array(X_validation).reshape(
        (X_validation.shape[0], int(X_validation.shape[1] / NUM_OF_MFCSS), NUM_OF_MFCSS))
    y_train = np.array(y_train)
    y_test = np.array(y_test)
    y_validation = np.array(y_validation)
    return X_train, X_test, X_validation, y_train, y_test, y_validation


def createCNNModel_EmoDB(inputShape, learn_rate=0.00005, dropout_rate=0):
    model = keras.Sequential()
    # 1st conv layer
    model.add(keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=inputShape))
    model.add(keras.layers.MaxPooling2D((3, 3), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())

    # 2nd conv layer
    model.add(keras.layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D((3, 3), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())

    # # 3rd conv layer
    # model.add(keras.layers.Conv2D(128, (2, 2), activation='relu'))
    # model.add(keras.layers.MaxPooling2D((2, 2), strides=(2, 2), padding='same'))
    # model.add(keras.layers.BatchNormalization())

    # flatten output and feed it into dense layer
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(128, activation='relu'))
    model.add(keras.layers.Dropout(dropout_rate))
    model.add(keras.layers.Dense(6, activation='softmax'))
    optimizer = keras.optimizers.Adam(learning_rate=learn_rate)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model


def createCNNModel_RavDB(inputShape, learn_rate=0.0001):
    model = keras.Sequential()
    # 1st conv layer
    model.add(keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=inputShape))
    model.add(keras.layers.MaxPooling2D((3, 3), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Dropout(0.1))

    # 2nd conv layer
    model.add(keras.layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D((3, 3), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Dropout(0.1))

    # flatten output and feed it into dense layer
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(128, activation='relu'))
    model.add(keras.layers.Dropout(0.3))
    model.add(keras.layers.Dense(6, activation='softmax'))
    optimizer = keras.optimizers.Adam(learning_rate=learn_rate)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model


def getAugData(y_train):
    result = pd.DataFrame()
    paths = []
    labels = []
    features = pd.DataFrame(columns=["features"])
    i = 0
    for index, filepath in enumerate(y_train["filepath"]):
        labelSeries = y_train.iloc[index, 1:7]
        label = ""
        for index1, x in enumerate(labelSeries):
            if x == 1:
                label = labelSeries.index[index1]
        org = getMFCCs(filepath)
        paths.append(filepath)
        labels.append(label)
        features.loc[i] = [org.flatten()]
        i = i + 1
        x = getMFCCsFromData(add_white_noise(filepath))
        paths.append(filepath)
        labels.append(label)
        features.loc[i] = [x.flatten()]
        i = i + 1
        y = getMFCCsFromData(pitch_scale(filepath))
        paths.append(filepath)
        labels.append(label)
        features.loc[i] = [y.flatten()]
        i = i + 1
        z = getMFCCsFromData(random_gain(filepath))
        paths.append(filepath)
        labels.append(label)
        features.loc[i] = [z.flatten()]
        i = i + 1
        print(i, "/", len(y_train) * 4)
    result["filepath"] = paths
    result["label"] = labels
    result = pd.concat([result, pd.DataFrame(features["features"].values.tolist())], axis=1)
    return getOneHotEncodedDataFrame(result)


data = pd.read_csv('./databases/RAVDESSDBTABLE_MFCC_DL.csv')
# data = data.drop(["filepath"], axis=1)

reports = []
for i in range(0, 20):
    X_train, X_test, y_train, y_test = train_test_split(data.iloc[:, 7:], data.iloc[:, 0:7], test_size=0.2)
    X_train, X_validation, y_train, y_validation = train_test_split(X_train, y_train, test_size=0.3)
    augmentedData = getAugData(y_train).drop(["filepath"], axis=1).sample(frac=1)
    X_train = augmentedData.iloc[:, :-6]
    y_train = augmentedData.iloc[:, -6:]
    # y_train = y_train.drop(["filepath"], axis=1)
    y_test = y_test.drop(["filepath"], axis=1)
    y_validation = y_validation.drop(["filepath"], axis=1)
    input_shape = (int(X_train.to_numpy().shape[1] / NUM_OF_MFCSS), NUM_OF_MFCSS, 1)
    # *-------------------------------------------------------------------------------------------------------------------*
    X_train, X_test, X_validation, y_train, y_test, y_validation = reshapeSets(X_train, X_test, X_validation, y_train,
                                                                               y_test, y_validation)

    # RavDB hyperparameters tuned
    model = createCNNModel_RavDB(input_shape)
    # model.summary()
    history = model.fit(X_train, y_train, batch_size=64, epochs=20, validation_data=(X_validation, y_validation))
    predictions = np.argmax(model.predict(X_test), axis=1)
    y_test = np.argmax(y_test, axis=1)
    report = classification_report(y_test, predictions, output_dict=True)
    reports.append(report)
    print("INDEX: ", i)

outputFile = open("./results/cnn_rav.json", "w")
json.dump(reports, outputFile, indent=4)
outputFile.close()

# EmoDB hyperparameters tuned
# model = createCNNModel_EmoDB(input_shape)
# model.summary()
# history = model.fit(X_train, y_train, batch_size=32, epochs=15, validation_data=(X_validation, y_validation))

# RavDB hyperparameters tuned
# model = createCNNModel_RavDB(input_shape)
# # model.summary()
# history = model.fit(X_train, y_train, batch_size=64, epochs=20, validation_data=(X_validation, y_validation))

# # plot_history(history)
# # evaluate model on test set
# test_loss, test_acc = model.evaluate(X_test, y_test, verbose=2)
# print('\nTest accuracy:', test_acc)
# # pick a sample to predict from the test set
# X_to_predict = X_test[15]
# y_to_predict = y_test[15]
# # predict sample
# predict(model, X_to_predict, y_to_predict)
