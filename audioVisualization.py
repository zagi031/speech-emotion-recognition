import warnings

import librosa
import librosa.display
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

warnings.filterwarnings('ignore')


def showWaveplot(data, sr, emotion):
    plt.figure(figsize=(10, 4))
    plt.title(emotion, size=20)
    librosa.display.waveplot(data, sr=sr)
    plt.show()


def showSpectogram(data, sr, emotion):
    x = librosa.stft(data)
    xdb = librosa.amplitude_to_db(abs(x))
    plt.figure(figsize=(10, 7))
    plt.title(emotion, size=20)
    librosa.display.specshow(xdb, sr=sr, x_axis='time', y_axis='hz')
    plt.colorbar().set_label('dB')
    plt.show()


def visualizeMFCCs(data, sr, emotion):
    x = librosa.feature.mfcc(data, sr, n_mfcc=20)
    plt.figure(figsize=(11, 4))
    plt.title(emotion, size=20)
    librosa.display.specshow(x, sr=sr, x_axis='time')
    plt.ylabel("MFCC indeks")
    plt.xlabel("vrijeme")
    plt.colorbar()
    plt.show()


emotion = 'ljutit govor'
path = np.array(pd.read_csv('./databases/RAVDESSDBTable_MFCC_ML.csv')['filepath'])[0]
data, sampling_rate = librosa.load(path)
print(data.shape)
showWaveplot(data, sampling_rate, emotion)
showSpectogram(data, sampling_rate, emotion)
visualizeMFCCs(data, sampling_rate, emotion)
