import pandas as pd


def getLabelFromLabelAbbrevation_EmoDB(labelAbbrevation):
    if labelAbbrevation == "W":
        return "anger"
    elif labelAbbrevation == "E":
        return "disgust"
    elif labelAbbrevation == "A":
        return "fear"
    elif labelAbbrevation == "F":
        return "happy"
    elif labelAbbrevation == "N":
        return "neutral"
    elif labelAbbrevation == "T":
        return "sad"
    else:
        return "unknown"


def getLabelFromLabelAbbrevation_RAVDESSDB(labelAbbrevation):
    if labelAbbrevation == "05":
        return "anger"
    elif labelAbbrevation == "07":
        return "disgust"
    elif labelAbbrevation == "06":
        return "fear"
    elif labelAbbrevation == "03":
        return "happy"
    elif labelAbbrevation == "01":
        return "neutral"
    elif labelAbbrevation == "04":
        return "sad"
    else:
        return "unknown"


def getOneHotEncodedDataFrame(dataFrame):
    oneHotEncoding = pd.get_dummies(dataFrame.label)
    result = pd.concat([dataFrame, oneHotEncoding], axis=1)
    del result["label"]
    return result
