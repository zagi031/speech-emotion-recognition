import pandas as pd
from sklearn.preprocessing import MinMaxScaler

data = pd.read_csv('./databases/EmoDBTable_MFCC_DL.csv')
features = data.iloc[:, 7:]
data = data.iloc[:, :7]
scaler = MinMaxScaler()
scaler.fit(features)
scaled = scaler.fit_transform(features)
scaledFeatures = pd.DataFrame(scaled, columns=features.columns)
pd.concat([data, scaledFeatures], axis=1).to_csv('./databases/EmoDBTable_MFCC_DL_NORMALIZED.csv', index=False)
