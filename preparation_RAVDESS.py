import os

import pandas as pd
import numpy as np
from functions_dbPreparation import getLabelFromLabelAbbrevation_RAVDESSDB, getOneHotEncodedDataFrame
from functions_preprocessing import getMFCCs, getMeanMFCCs, amplifyVolume, getMelSpectrogramValues, \
    getMeanMelSpectrogramValues

RAVDESSorginalDBpath = "./databases/RAVDESS_orginal/"
RAVDESSDBpath = "./databases/RAVDESS/"
RAVDESSDBfilePaths = []
RAVDESSDBlabels = []
RAVDESSDBtable = pd.DataFrame()
features = pd.DataFrame(columns=["features"])

for subdir, dirs, files in os.walk(RAVDESSorginalDBpath):
    for file in files:
        labelAbbrevation = file[6:8]
        label = getLabelFromLabelAbbrevation_RAVDESSDB(labelAbbrevation)
        if label != "unknown":
            audioFile = amplifyVolume(subdir + "/" + file)
            path = RAVDESSDBpath + file
            audioFile.export(path, format="wav")
            RAVDESSDBfilePaths.append(path)
            RAVDESSDBlabels.append(label)

RAVDESSDBtable["filepath"] = RAVDESSDBfilePaths
RAVDESSDBtable["label"] = RAVDESSDBlabels
# RAVDESSDBtable = getOneHotEncodedDataFrame(RAVDESSDBtable)

for index, filepath in enumerate(RAVDESSDBtable.filepath):
    # Deep learning preparation
    # mfccValues = getMFCCs(filepath)
    # melFeatures = getMelSpectrogramValues(filepath)
    # features.loc[index] = [melFeatures.flatten()]

    # Classic machine learning preparation
    # meanMFCCs = getMeanMFCCs(filepath)
    meanMelFeatures = getMeanMelSpectrogramValues(filepath)
    features.loc[index] = [meanMelFeatures]
    print(index)

RAVDESSDBtable = pd.concat([RAVDESSDBtable, pd.DataFrame(features["features"].values.tolist())], axis=1)
# Saving data for deep learning
# RAVDESSDBtable.to_csv('./databases/RAVDESSDBTable_MFCC_DL.csv', index=False)
# Saving data for classic machine learning
RAVDESSDBtable.to_csv('./databases/RAVDESSDBTable_MEL_ML.csv', index=False)
