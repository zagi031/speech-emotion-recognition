import json
import joblib
import pandas as pd
from sklearn import svm
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, log_loss, precision_score, \
    recall_score, f1_score
from sklearn.model_selection import KFold, train_test_split
from functionsPerformanceAnalysys import plotCFMatrix, printBestParamsFromHyperparameterTuning, \
    plotCrossValidationAnalysys, printMeanAndStdDevResults

data = pd.read_csv("./databases/RAVDESSDBTable_MFCC_ML.csv")
target = data["label"]
data = data.drop(["label", "filepath"], axis=1)

reports = []
for i in range(0, 20):
    X_data, X_test, y_data, y_test = train_test_split(data, target, test_size=0.2, shuffle=True, stratify=target)

    # training_accuracy_scores = []
    # training_log_loss_scores = []
    # validation_accuracy_scores = []
    # validation_log_loss_scores = []
    #
    # param_grid = {'C': [10, 100, 1000, 10000, 100000],
    #               'gamma': [0.001, 0.0001, 0.00001, 0.000001, 0.0000001],
    #               'kernel': ['rbf']}
    #
    # # hyperparameter tuning
    # # printBestParamsFromHyperparameterTuning(param_grid, svm.SVC(probability=True), X_data, y_data, 25)
    # # MFCC
    # # Emo-DB {'kernel': 'rbf', 'gamma': 1e-06, 'C': 10000}
    # # RAVDESS {'kernel': 'rbf', 'gamma': 0.0001, 'C': 1000}
    # # MEL
    # # Emo-DB {'kernel': 'rbf', 'gamma': 1e-05, 'C': 100}
    # # RAVDESS {'kernel': 'rbf', 'gamma': 0.001, 'C': 10}
    #
    # # training + validation
    # kf = KFold(n_splits=5, shuffle=True, random_state=None)
    # for train_index, validation_index in kf.split(X_data):
    #     X_train, y_train = data.iloc[train_index], target.iloc[train_index]
    #     X_validation, y_validation = data.iloc[validation_index], target.iloc[validation_index]
    #
    #     classifier = svm.SVC(C=100, gamma=0.00001, probability=True)
    #     classifier.fit(X_train, y_train)
    #
    #     predictions = classifier.predict(X_validation)
    #     accuracy = accuracy_score(y_validation, predictions)
    #     print(accuracy)
    #     validation_accuracy_scores.append(accuracy)
    #     training_accuracy_scores.append(accuracy_score(y_train, classifier.predict(X_train)))
    #     training_log_loss_scores.append(log_loss(y_train, classifier.predict_proba(X_train)))
    #     validation_log_loss_scores.append(log_loss(y_validation, classifier.predict_proba(X_validation)))
    #
    # plotCrossValidationAnalysys(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
    #                             validation_log_loss_scores, 5)
    # printMeanAndStdDevResults(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
    #                           validation_log_loss_scores)

    # testing
    # Emo-DB {'kernel': 'rbf', 'gamma': 1e-06, 'C': 10000}
    # RAVDESS {'kernel': 'rbf', 'gamma': 0.0001, 'C': 1000}
    classifier = svm.SVC(C=1000, gamma=0.0001, probability=True)
    classifier.fit(X_data, y_data)
    predictions = classifier.predict(X_test)
    report = classification_report(y_test, predictions, output_dict=True)
    reports.append(report)
    print(i)

outputFile = open("./results/svm_rav.json", "w")
json.dump(reports, outputFile, indent=4)
outputFile.close()
# joblib.dump(classifier, "svmClassifier.pkl")
# classifier = joblib.load("svmClassifier.pkl")


# predictions = classifier.predict(X_test)
# print(classification_report(y_test, predictions))
# cf_matrix = confusion_matrix(y_test, predictions)
# # print(cf_matrix)
# plotCFMatrix(cf_matrix, classifier.classes_)
