import joblib
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, log_loss
from sklearn.model_selection import KFold, train_test_split
from functionsPerformanceAnalysys import plotCFMatrix, printBestParamsFromHyperparameterTuning, \
    plotCrossValidationAnalysys, printMeanAndStdDevResults

data = pd.read_csv("./databases/EmoDBTABLE_MEL_ML.csv")
target = data["label"]
data = data.drop(["label", "filepath"], axis=1)
X_data, X_test, y_data, y_test = train_test_split(data, target, test_size=0.2,
                                                  random_state=0, shuffle=True, stratify=target)

training_accuracy_scores = []
training_log_loss_scores = []
validation_accuracy_scores = []
validation_log_loss_scores = []

param_grid = {'n_neighbors': list(range(1, 50)),
              'leaf_size': list(range(1, 30)),
              'p': [1, 2]}

# hyperparameter tuning
# printBestParamsFromHyperparameterTuning(param_grid, KNeighborsClassifier(), X_data, y_data, 1000)
# MFCC
# Emo-DB {'p': 1, 'n_neighbors': 20, 'leaf_size': 14}
# RAVDESS {'p': 1, 'n_neighbors': 36, 'leaf_size': 1}
# MEL {'p': 2, 'n_neighbors': 27, 'leaf_size': 27}
# Emo-DB {'kernel': 'rbf', 'gamma': 1e-05, 'C': 100}
# RAVDESS {'p': 1, 'n_neighbors': 40, 'leaf_size': 22}

# training + validation
kf = KFold(n_splits=5, shuffle=True, random_state=None)
for train_index, validation_index in kf.split(data):
    X_train, y_train = data.iloc[train_index], target.iloc[train_index]
    X_validation, y_validation = data.iloc[validation_index], target.iloc[validation_index]

    classifier = KNeighborsClassifier(n_neighbors=27, leaf_size=27, p=2)
    classifier.fit(X_train, y_train)

    predictions = classifier.predict(X_validation)
    accuracy = accuracy_score(y_validation, predictions)
    print(accuracy)
    validation_accuracy_scores.append(accuracy)
    training_accuracy_scores.append(accuracy_score(y_train, classifier.predict(X_train)))
    training_log_loss_scores.append(log_loss(y_train, classifier.predict_proba(X_train)))
    validation_log_loss_scores.append(log_loss(y_validation, classifier.predict_proba(X_validation)))

plotCrossValidationAnalysys(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
                            validation_log_loss_scores, 5)
printMeanAndStdDevResults(training_accuracy_scores, validation_accuracy_scores, training_log_loss_scores,
                          validation_log_loss_scores)

# testing
classifier = KNeighborsClassifier(n_neighbors=27, leaf_size=27, p=2)
classifier.fit(X_data, y_data)
joblib.dump(classifier, "svmClassifier.pkl")
# classifier = joblib.load("svmClassifier.pkl")

predictions = classifier.predict(X_test)
print(classification_report(y_test, predictions))
cf_matrix = confusion_matrix(y_test, predictions)
# print(cf_matrix)
plotCFMatrix(cf_matrix, classifier.classes_)
